var needle = require('needle');
var request = require('request');
var cheerio = require('cheerio');
var url = require('url');
var fs = require('fs');

var BASE_URL = 'https://prnt.sc/';


var download = function(uri, filename, callback){
    request(uri).pipe(fs.createWriteStream('images/' + filename)).on('close', callback);
};

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
      end = new Date().getTime();
   }
 }

function checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
}

function time() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();

    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);

    return "[" + today.getFullYear() + "-" + checkTime(today.getMonth() + 1) + "-" + checkTime(today.getDate()) + " " + h + ":" + m + ":" + s + "] ";
}

function main() {
    alphabet = '0123456789abcdefghijklmnopqrstuvwxyz';

    mUrl = '';
    for(let i = 0; i < 6; i++)
        mUrl += alphabet[Math.floor(Math.random()*alphabet.length)];

    let URL = BASE_URL + mUrl;

    console.log(time(), 'Parsing site: ', URL);
    needle.get(URL, function (err, res) {
        if (err) {
            console.log(time(), 'Some error: ', URL);
            return main();
        }

        console.log(res.statusCode);
        if (res.statusCode != 200) {
            console.log(time(), 'Cant get site: ', URL);
            return main();
        }

        var $ = cheerio.load(res.body);

        if ($('#screenshot-image') == undefined) {
            console.log(time(), 'Cant get image :(');
            return main();
        }

        let src = $('#screenshot-image').attr('src');

        if(src[0] == '/') {
            console.log(time(), 'Bad image source');
            return main();
        }

        let filename = url.parse(src).pathname.substr(url.parse(src).pathname.lastIndexOf('/') + 1);
        console.log(time(), 'src: ', src);
        console.log(time(), 'filename: ', filename);

        download(src, filename, function () {
            console.log(time(), 'Saved as: ', filename);
        });

        return main();
    });
}

main();


