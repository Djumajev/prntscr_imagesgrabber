from PIL import Image
import numpy
import os, sys
import os 


def resize(dir):
    path = os.getcwd() + dir
    dirs = os.listdir( path )
    for item in dirs:
        if os.path.isfile(path+item):
            im = Image.open(path+item)
            f, e = os.path.splitext(path+item)
            imResize = im.resize((28,28), Image.ANTIALIAS)
            imResize = imResize.convert('1') # convert image to black and white
            imResize.save(f + e, 'JPEG', quality=90)
        else:
            print('Not an item: ' + path+dir+item)


resize("/images_process/")

def think(dir):
    path = os.getcwd() + dir
    dirs = os.listdir( path )
    for item in dirs:
        if os.path.isfile(path+item):
            im = Image.open(path+item)
            arr = numpy.array(im)
            sum = 0
            for i in arr:
                for e in i:
                    sum += e
            if sum > (28*28 * 255) * 0.4:
                print(item)
            

think("/images_process/")
